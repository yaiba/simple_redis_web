#!/usr/bin/env python
#coding:utf8

import os

import web
import redis

REDIS_CONF = {
         "host": os.environ.get("REDIS_HOST", "") or "localhost",
         "port": os.environ.get("REDIS_PORT", 0) or 6379,
         "db": os.environ.get("REDIS_DB", 0) or 0,
         #"password": "",
         }
REDIS_CONN = redis.Redis(**REDIS_CONF)

urls = (
    '/', 'index',
    '/incr', 'incr',
    '/decr', 'decr',
    '/count', 'count',
)


class incr(object):
    def GET(self):
        REDIS_CONN.incr('view')
        return 'incr view'


class decr(object):
    def GET(self):
        REDIS_CONN.decr('view')
        return 'decr view'


class index(object):
    def GET(self):
        REDIS_CONN.ping()
        REDIS_CONN.echo('hey yaiba')
        return 'demo index'


class count(object):
    def GET(self,):
        return 'view %s' % REDIS_CONN.get('view')


app = web.application(urls, globals(), autoreload=True)
if __name__ == "__main__":
        app.run()
